package com.quoc.coroutine.util

import org.junit.Assert.*
import org.junit.Test

class EmailUtilsTest {

    @Test
    fun email_null() {
        assertFalse(EmailUtils.isEmailFormat(null))
    }

    @Test
    fun email_empty() {
        assertFalse(EmailUtils.isEmailFormat(""))
    }

    @Test
    fun email_incorrect_format() {
        assertFalse(EmailUtils.isEmailFormat("Aaaaaaaaaaa"))
    }

    @Test
    fun email_incorrect_suffix() {
        assertFalse(EmailUtils.isEmailFormat("abc@gmail.com"))
    }

    @Test
    fun email_correct() {
        assertTrue(EmailUtils.isEmailFormat("abc@positivethinking.tech"))
    }
}