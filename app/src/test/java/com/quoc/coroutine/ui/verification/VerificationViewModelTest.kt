package com.quoc.coroutine.ui.verification

import androidx.lifecycle.SavedStateHandle
import app.cash.turbine.test
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.TimeUnit
import kotlin.time.ExperimentalTime

@ExperimentalTime
@FlowPreview
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class VerificationViewModelTest {

    @Mock
    lateinit var state: SavedStateHandle
    private lateinit var viewModel: VerificationViewModel
    private val expectedOtpCode = "123456"
    private val keyOtpCode: String = "otp_code"

    @Before
    fun setUp() {
        viewModel = VerificationViewModel(state)
    }

    @Test
    fun verifyOtp_empty() = runBlockingTest {
        val otpCode = ""
        viewModel.verifyOtp(otpCode)
        TimeUnit.MILLISECONDS.sleep(2200)
        val data = viewModel.isOtpCodeValid.value
        assertFalse(data)
    }

    @Test
    fun verifyOtp_invalid() = runBlockingTest {
        val otpCode = "13567"
        viewModel.verifyOtp(otpCode)
        TimeUnit.MILLISECONDS.sleep(2200)
        val data = viewModel.isOtpCodeValid.value
        assertFalse(data)
    }

    @Test
    fun verifyOtp_valid() = runBlockingTest {
        val otpCode = "123456"
        viewModel.verifyOtp(otpCode)
        TimeUnit.MILLISECONDS.sleep(2200)
        val data = viewModel.isOtpCodeValid.value
        assertTrue(data)
    }

    @Test
    fun submit_error() = runBlockingTest {
        Mockito.`when`(state.get<String>(keyOtpCode)).thenReturn(expectedOtpCode)
        val otpCode = "123457"
        viewModel.verifyOtp(otpCode)
        TimeUnit.MILLISECONDS.sleep(2200)
        viewModel.validateSucceeded.test {
            viewModel.submit()
            val data = awaitItem()
            assertFalse(data)
            cancelAndConsumeRemainingEvents()
        }
    }

    @Test
    fun submit_succeeded() = runBlockingTest {
        Mockito.`when`(state.get<String>(keyOtpCode)).thenReturn(expectedOtpCode)
        val otpCode = "123456"
        viewModel.verifyOtp(otpCode)
        TimeUnit.MILLISECONDS.sleep(2200)
        viewModel.validateSucceeded.test {
            viewModel.submit()
            val data = awaitItem()
            assertTrue(data)
            cancelAndConsumeRemainingEvents()
        }
    }

}