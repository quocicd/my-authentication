package com.quoc.coroutine.ui.login

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.TimeUnit
import kotlin.time.ExperimentalTime

@ExperimentalTime
@FlowPreview
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    private lateinit var viewModel: LoginViewModel

    @Before
    fun setUp() {
        viewModel = LoginViewModel()
    }

    @Test
    fun checkEmail_empty() = runBlockingTest {
        val email = ""
        viewModel.checkEmail(email)
        TimeUnit.MILLISECONDS.sleep(2200)
        val data = viewModel.isEmailValid.value
        assertFalse(data)
        assertEquals(email, viewModel.email.value)
        assertEquals("", viewModel.otpCode)
    }

    @Test
    fun checkEmail_invalid() = runBlockingTest {
        val email = "hello@gmail.com"
        viewModel.checkEmail(email)
        TimeUnit.MILLISECONDS.sleep(2200)
        val data = viewModel.isEmailValid.value
        assertFalse(data)
        assertEquals(email, viewModel.email.value)
        assertEquals("", viewModel.otpCode)
    }

    @Test
    fun checkEmail_valid() = runBlockingTest {
        val email = "abc@positivethinking.tech"
        viewModel.checkEmail(email)
        TimeUnit.MILLISECONDS.sleep(2200)
        val data = viewModel.isEmailValid.value
        assertTrue(data)
        assertEquals(email, viewModel.email.value)
        assertEquals("123456", viewModel.otpCode)
    }
}