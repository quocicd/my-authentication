package com.quoc.coroutine.ui.login

import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import com.quoc.coroutine.BaseIdlingResourceTest
import com.quoc.coroutine.R
import com.quoc.coroutine.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.verify
import java.util.concurrent.TimeUnit

@FlowPreview
@ExperimentalCoroutinesApi
@MediumTest
@HiltAndroidTest
class LoginFragmentTest : BaseIdlingResourceTest() {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    override fun setUp() {
        super.setUp()
        hiltRule.inject()
        launchFragmentInHiltContainer<LoginFragment> {
            Navigation.setViewNavController(this.requireView(), navController)
        }
    }

    @Test
    fun invalidEmail_buttonDisabled() {
        onView(withId(R.id.edtEmail)).perform(typeText(""))
        TimeUnit.MILLISECONDS.sleep(2200)
        onView(withId(R.id.btnOtpCode)).check(matches(isNotEnabled()))
    }

    @Test
    fun validEmail_buttonEnabled() {
        onView(withId(R.id.edtEmail)).perform(typeText("abc@positivethinking.tech"))
        TimeUnit.MILLISECONDS.sleep(2200)
        onView(withId(R.id.btnOtpCode)).check(matches(isEnabled()))
    }

    @Test
    fun validEmail_pressButton_navigateToVerification() {
        val email = "abc@positivethinking.tech"
        onView(withId(R.id.edtEmail)).perform(typeText(email))
        TimeUnit.MILLISECONDS.sleep(2200)
        onView(withId(R.id.btnOtpCode)).check(matches(isEnabled()))
        pressBack() // To hide keyboard
        TimeUnit.SECONDS.sleep(1)
        onView(withId(R.id.btnOtpCode)).perform(click())
        verify(navController).navigate(LoginFragmentDirections.verify("123456", email))
    }
}