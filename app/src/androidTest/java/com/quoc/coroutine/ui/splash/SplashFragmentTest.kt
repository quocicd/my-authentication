package com.quoc.coroutine.ui.splash

import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import com.quoc.coroutine.BaseIdlingResourceTest
import com.quoc.coroutine.R
import com.quoc.coroutine.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

@ExperimentalCoroutinesApi
@MediumTest
@HiltAndroidTest
class SplashFragmentTest : BaseIdlingResourceTest() {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    override fun setUp() {
        super.setUp()
        hiltRule.inject()
    }

    @Test
    fun loadingLogo_and_navigateToHome() {
        val navController = mock(NavController::class.java)
        launchFragmentInHiltContainer<SplashFragment> {
            Navigation.setViewNavController(this.requireView(), navController)
        }

        onView(withId(R.id.ivLogo)).check(matches(isDisplayed()))
        verify(navController).navigate(SplashFragmentDirections.login())
    }

}