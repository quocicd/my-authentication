package com.quoc.coroutine.ui.verification

import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.MediumTest
import com.quoc.coroutine.BaseIdlingResourceTest
import com.quoc.coroutine.R
import com.quoc.coroutine.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.verify
import java.util.concurrent.TimeUnit

@FlowPreview
@ExperimentalCoroutinesApi
@MediumTest
@HiltAndroidTest
class VerificationFragmentTest : BaseIdlingResourceTest() {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private val params = bundleOf(
        "email" to "abc@positivethinking.tech",
        "otp_code" to "123456"
    )

    override fun setUp() {
        super.setUp()
        hiltRule.inject()

        launchFragmentInHiltContainer<VerificationFragment>(
            fragmentArgs = params
        ) {
            Navigation.setViewNavController(this.requireView(), navController)
        }
    }

    @Test
    fun invalidOtpCode_buttonDisabled() {
        onView(withId(R.id.edtOptCode)).perform(typeText("12568"))
        TimeUnit.MILLISECONDS.sleep(2200)
        onView(withId(R.id.btnSubmit)).check(matches(isNotEnabled()))
    }

    @Test
    fun validOtpCode_buttonEnabled() {
        onView(withId(R.id.edtOptCode))
            .perform(typeText("123457"))
        TimeUnit.MILLISECONDS.sleep(2200)
        onView(withId(R.id.btnSubmit)).check(matches(isEnabled()))
    }

    @Test
    fun incorrectOtpCode_pressButton_showError() {
        onView(withId(R.id.edtOptCode)).perform(typeText("123458"))
        TimeUnit.MILLISECONDS.sleep(2200)
        onView(withId(R.id.btnSubmit)).check(matches(isEnabled()))
        Espresso.pressBack() // To hide keyboard
        TimeUnit.SECONDS.sleep(1)
        onView(withId(R.id.btnSubmit)).perform(ViewActions.click())
        TimeUnit.SECONDS.sleep(1)
        onView(withText("OTP is invalid")).check(matches(isDisplayed()))
    }

    @Test
    fun incorrectOtpCode_pressButton_navigateToHome() {
        onView(withId(R.id.edtOptCode)).perform(typeText("123456"))
        TimeUnit.MILLISECONDS.sleep(2200)
        onView(withId(R.id.btnSubmit)).check(matches(isEnabled()))
        Espresso.pressBack() // To hide keyboard
        TimeUnit.SECONDS.sleep(1)
        onView(withId(R.id.btnSubmit)).perform(ViewActions.click())
        verify(navController).navigate(VerificationFragmentDirections.home(params.getString("email")!!))
    }
}