package com.quoc.coroutine.ui.home

import androidx.core.os.bundleOf
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.filters.LargeTest
import com.quoc.coroutine.BaseIdlingResourceTest
import com.quoc.coroutine.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
@LargeTest
@HiltAndroidTest
class HomeFragmentTest : BaseIdlingResourceTest() {

    private val params = bundleOf("email" to "abc@positivethinking.tech")

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    override fun setUp() {
        super.setUp()
        hiltRule.inject()
        launchFragmentInHiltContainer<HomeFragment>(
            fragmentArgs = params
        ) {}
    }

    @Test
    fun display_congratulation() {
        val text =
            "Congratulation ${params.getString("email")}. You have just finished the Simple Use Case"
        onView(withText(text)).check(matches(isDisplayed()))
    }
}