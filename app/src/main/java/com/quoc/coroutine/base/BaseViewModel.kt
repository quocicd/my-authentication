package com.quoc.coroutine.base

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.quoc.coroutine.lib.IsLoading
import com.quoc.coroutine.util.parseMessage
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var context: Application

    private var loadingCount = 0

    private val _networkState = MutableSharedFlow<Boolean>()
    val networkState: SharedFlow<Boolean>
        get() = _networkState

    private val _isLoading = MutableSharedFlow<Boolean>()
    val isLoading: SharedFlow<IsLoading>
        get() = _isLoading

    private val _error = MutableSharedFlow<String>()
    val error: SharedFlow<String>
        get() = _error

    private val _isUnauthorized = MutableSharedFlow<Boolean>()
    val isUnauthorized: SharedFlow<Boolean>
        get() = _isUnauthorized

    protected suspend fun showLoading() {
        if (loadingCount == 0) {
            _isLoading.emit(true)
        }
        ++loadingCount
    }

    protected suspend fun hideLoading() {
        --loadingCount
        if (loadingCount < 0) loadingCount = 0
        if (loadingCount == 0) {
            _isLoading.emit(false)
        }
    }

    fun updateNetworkState(isAvailable: Boolean) {
        viewModelScope.launch {
            _networkState.emit(isAvailable)
        }
    }

    protected suspend fun handleError(throwable: Throwable) {
        val (isUnauthorized, message) = throwable.parseMessage(context)
        if (isUnauthorized) {
            _isUnauthorized.emit(true)
        } else {
            if (message.isNotBlank()) {
                _error.emit(message)
            }
        }
    }

}