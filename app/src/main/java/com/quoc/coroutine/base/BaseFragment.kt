package com.quoc.coroutine.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.viewbinding.ViewBinding
import com.quoc.coroutine.R
import com.quoc.coroutine.extension.hideSoftKeyboard
import com.quoc.coroutine.receiver.NetworkReceiver
import com.quoc.coroutine.receiver.NetworkStateListener
import com.quoc.coroutine.ui.dialog.DialogManager
import com.quoc.coroutine.util.autoCleared
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

abstract class BaseFragment<VB : ViewBinding> : Fragment(), IBaseFragment, NetworkStateListener {

    @Inject
    lateinit var dialogManager: DialogManager

    @Inject
    lateinit var networkReceiver: NetworkReceiver

    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> VB

    var binding by autoCleared<VB>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return bindingInflater.invoke(inflater, container, false).apply {
            binding = this
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (this as? IBaseFragment)?.let {
            setupView()
            bindViewEvents()
            bindViewModel()
        }
        bindBaseEvents()
        initData()
    }

    override fun onNetworkStateChanged(isAvailable: Boolean){
        lifecycleScope.launch(Dispatchers.Main){
            viewModel.updateNetworkState(isAvailable)
        }
    }

    private fun bindBaseEvents(){
        networkReceiver.register(this)
        view?.setOnClickListener {
            hideSoftKeyboard()
        }
        viewModel.isUnauthorized bindTo ::onUnauthorized
    }

    private fun onUnauthorized(isUnauthorized: Boolean){
        if(isUnauthorized) {
            dialogManager.showDialogTwoButton(
                getString(R.string.unauthorized_title),
                getString(R.string.unauthorized_message)
            ) { _, _ ->
                handleUnauthorized()
            }
        }
    }

    private fun handleUnauthorized(){
        // TODO clear cache
    }

    // Step 2
    protected open fun initData() = Unit


    protected fun toast(msg: String?) {
        dialogManager.toast(msg)
    }

    protected fun error(msg: String){
        dialogManager.showDialogOneButton(msg)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        networkReceiver.unregister()
    }

    protected fun isNetworkAvailable() = networkReceiver.isNetworkAvailable()

    protected inline infix fun <T> Flow<T>.bindTo(crossinline action: (T) -> Unit){
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED){
                collect {
                    action.invoke(it)
                }
            }
        }
    }

}