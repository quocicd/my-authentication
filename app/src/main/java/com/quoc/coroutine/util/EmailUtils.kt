package com.quoc.coroutine.util

import androidx.core.util.PatternsCompat

object EmailUtils {

    fun isEmailFormat(email: String?): Boolean =
        !email.isNullOrBlank()
                && email.endsWith("@positivethinking.tech", false)
                && PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()

}