package com.quoc.coroutine.receiver

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import javax.inject.Inject

class NetworkReceiver @Inject constructor(
    @ApplicationContext private val context: Context
) {
    private var connectivityManager: ConnectivityManager? = null
    private var listener: NetworkStateListener? = null
    private var networkCallback: ConnectivityManager.NetworkCallback? = null

    fun register(listener: NetworkStateListener) {
        connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        register()
        this.listener = listener
    }

    private fun register() {
        val netRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()

        networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                notifyListeners()
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                notifyListeners()
            }
        }

        connectivityManager?.registerNetworkCallback(netRequest, networkCallback!!)
    }

    fun unregister() {
        try {
            connectivityManager?.unregisterNetworkCallback(networkCallback ?: return)
        } catch (e: IllegalArgumentException) {
            Timber.e("Network callback was unregistered before")
        }
    }

    private fun notifyListeners() {
        listener?.onNetworkStateChanged(isNetworkAvailable())
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

}

interface NetworkStateListener {
    fun onNetworkStateChanged(isAvailable: Boolean)
}