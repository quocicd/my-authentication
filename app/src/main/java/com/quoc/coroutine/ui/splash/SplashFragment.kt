package com.quoc.coroutine.ui.splash

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.quoc.coroutine.R
import com.quoc.coroutine.base.BaseFragment
import com.quoc.coroutine.databinding.FragmentSplashBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentSplashBinding
        get() = { layoutInflater, parent, attachedToParent ->
            FragmentSplashBinding.inflate(layoutInflater, parent, attachedToParent)
        }

    override val viewModel: SplashViewModel by viewModels()

    override fun setupView() {

    }

    override fun bindViewEvents() {

    }

    override fun bindViewModel() {
        viewModel.showLogo bindTo ::displayAnimation
        viewModel.navigateToHome bindTo ::navigateToHome
    }

    private fun displayAnimation(isShowLogo: Boolean) {
        binding.ivLogo.isVisible = isShowLogo
        binding.ivLogo.startAnimation(
            AnimationUtils.loadAnimation(
                requireContext(),
                R.anim.zoom_in
            )
        )
    }

    private fun navigateToHome(isEnabled: Boolean) {
        if (isEnabled) {
            findNavController().navigate(SplashFragmentDirections.login())
        }
    }
}
