package com.quoc.coroutine.ui.home

import androidx.lifecycle.SavedStateHandle
import com.quoc.coroutine.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    state: SavedStateHandle
) : BaseViewModel() {
    val email = state["email"] ?: ""
}