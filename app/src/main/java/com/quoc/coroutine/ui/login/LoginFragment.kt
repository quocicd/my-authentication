package com.quoc.coroutine.ui.login

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.quoc.coroutine.base.BaseFragment
import com.quoc.coroutine.databinding.FragmentLoginBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentLoginBinding
        get() = { layoutInflater, parent, attachedToParent ->
            FragmentLoginBinding.inflate(layoutInflater, parent, attachedToParent)
        }

    override val viewModel: LoginViewModel by viewModels()

    override fun setupView() {

    }

    override fun bindViewEvents() {
        binding.btnOtpCode.setOnClickListener {
            findNavController().navigate(
                LoginFragmentDirections.verify(
                    viewModel.otpCode,
                    viewModel.email.value,
                )
            )
        }

        binding.edtEmail.doAfterTextChanged {
            viewModel.checkEmail(it?.toString() ?: "")
        }
    }

    override fun bindViewModel() {
        viewModel.error bindTo ::toast
        viewModel.isLoading bindTo ::displayLoading
        viewModel.isEmailValid bindTo ::displayButton
    }

    private fun displayLoading(isLoading: Boolean){
        binding.progressBar.isVisible = isLoading
    }

    private fun displayButton(enabled: Boolean) {
        binding.btnOtpCode.isEnabled = enabled
    }
}
