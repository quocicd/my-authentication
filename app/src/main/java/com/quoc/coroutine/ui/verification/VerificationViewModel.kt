package com.quoc.coroutine.ui.verification

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.quoc.coroutine.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
@HiltViewModel
class VerificationViewModel @Inject constructor(
    private val state: SavedStateHandle
) : BaseViewModel() {

    val email = state["email"] ?: ""

    private val _isOtpCodeValid = MutableStateFlow(false)
    val isOtpCodeValid: StateFlow<Boolean> = _isOtpCodeValid

    private val _validateSucceeded = MutableSharedFlow<Boolean>()
    val validateSucceeded: SharedFlow<Boolean>
        get() = _validateSucceeded

    private val otpCode = MutableStateFlow("")

    init {
        viewModelScope.launch(Dispatchers.IO) {
            otpCode.debounce(500)
                .distinctUntilChanged()
                .flatMapLatest {
                    flow {
                        delay(1500)
                        emit(it.length == OTP_SIZE)
                    }.onStart {
                        if (it.isNotEmpty()) {
                            showLoading()
                        }
                    }.onCompletion { hideLoading() }
                }
                .collect {
                    _isOtpCodeValid.value = it
                }
        }
    }

    fun verifyOtp(code: String) {
        viewModelScope.launch {
            otpCode.value = code
        }
    }

    fun submit() {
        viewModelScope.launch {
            _validateSucceeded.emit(state.get<String>("otp_code") ?: "" == otpCode.value)
        }
    }

    companion object {
        const val OTP_SIZE = 6
    }

}
