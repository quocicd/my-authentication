package com.quoc.coroutine.ui.login

import androidx.lifecycle.viewModelScope
import com.quoc.coroutine.base.BaseViewModel
import com.quoc.coroutine.util.EmailUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@ExperimentalCoroutinesApi
@FlowPreview
@HiltViewModel
class LoginViewModel @Inject constructor() : BaseViewModel() {

    private val _isEmailValid = MutableStateFlow(false)
    val isEmailValid: StateFlow<Boolean> = _isEmailValid

    val email = MutableStateFlow("")
    var otpCode = ""

    init {
        viewModelScope.launch(Dispatchers.IO) {
            email.debounce(500)
                .distinctUntilChanged()
                .flatMapLatest {
                    flow {
                        delay(1500)
                        emit(EmailUtils.isEmailFormat(it))
                    }.onStart {
                        if (it.isNotEmpty()) {
                            showLoading()
                        }
                    }.onCompletion { hideLoading() }
                }
                .collect {
                    if (it) {
                        otpCode = OTP_CODE
                    }
                    _isEmailValid.value = it
                }
        }
    }

    fun checkEmail(emailAddress: String) {
        viewModelScope.launch {
            email.value = emailAddress
        }
    }

    companion object {
        const val OTP_CODE = "123456"
    }

}
