package com.quoc.coroutine.ui.verification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.quoc.coroutine.R
import com.quoc.coroutine.base.BaseFragment
import com.quoc.coroutine.databinding.FragmentVerificationBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@AndroidEntryPoint
class VerificationFragment : BaseFragment<FragmentVerificationBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentVerificationBinding
        get() = { layoutInflater, parent, attachedToParent ->
            FragmentVerificationBinding.inflate(layoutInflater, parent, attachedToParent)
        }

    override val viewModel: VerificationViewModel by viewModels()

    override fun setupView() {

    }

    override fun bindViewEvents() {
        binding.ivBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.edtOptCode.doAfterTextChanged {
            viewModel.verifyOtp(it?.toString() ?: "")
        }

        binding.btnSubmit.setOnClickListener {
            viewModel.submit()
        }
    }

    override fun bindViewModel() {
        viewModel.error bindTo ::toast
        viewModel.isOtpCodeValid bindTo ::displayButton
        viewModel.validateSucceeded bindTo ::validateSucceed
        viewModel.isLoading bindTo ::displayLoading
    }

    private fun displayLoading(isLoading: Boolean){
        binding.progressBar.isVisible = isLoading
    }

    private fun displayButton(enabled: Boolean) {
        binding.btnSubmit.isEnabled = enabled
    }

    private fun validateSucceed(isSucceeded: Boolean) {
        if (isSucceeded) {
            findNavController().navigate(VerificationFragmentDirections.home(viewModel.email))
        } else {
            error(getString(R.string.opt_code_is_invalid))
        }
    }

}
