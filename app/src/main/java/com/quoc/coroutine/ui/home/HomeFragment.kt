package com.quoc.coroutine.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.quoc.coroutine.R
import com.quoc.coroutine.base.BaseFragment
import com.quoc.coroutine.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override val viewModel: HomeViewModel by viewModels()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentHomeBinding
        get() = { inflater, parent, attachToParent ->
            FragmentHomeBinding.inflate(inflater, parent, attachToParent)
        }

    override fun setupView() {
        binding.tvStatus.text = getString(R.string.congratulation, viewModel.email)
    }

    override fun bindViewEvents() {

    }

    override fun bindViewModel() {
        viewModel.error bindTo ::toast
    }

}