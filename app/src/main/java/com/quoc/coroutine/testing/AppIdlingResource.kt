package com.quoc.coroutine.testing

import androidx.test.espresso.idling.CountingIdlingResource
import com.quoc.coroutine.BuildConfig

object AppIdlingResource {

    private const val RESOURCE = "GLOBAL"

    val counting = CountingIdlingResource(RESOURCE)

    fun increment() {
        if (BuildConfig.DEBUG) {
            counting.increment()
        }
    }

    fun decrement() {
        if (BuildConfig.DEBUG) {
            if (!counting.isIdleNow) {
                counting.decrement()
            }
        }
    }
}