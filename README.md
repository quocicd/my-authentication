## My Authentication

### Get started
My Authentication is an Android application that allows developers to show how to implement TDD with basic functionalities.

Application information:
- Application is written by Kotlin language
- MVVM architecture
- Libraries: Flow, Kotlin Coroutines, Hilt

### Install
1. Pull source code [My authentication](https://gitlab.com/quocicd/my-authentication).

```bash
git@gitlab.com:quocicd/my-authentication.git
```
2. Download and open [Android studio](https://developer.android.com/studio)
3. [Build and run](https://developer.android.com/studio/run) the application
4. Build a debug APK
```bash
gradlew assembleDebug
```
5. Install on a running emulator or a connected device
```bash
gradlew installDebug
```
6. Run unit test
```bash
gradlew testDebug
```
### Screens
#### Splash screen
- Show logo and navigate to login screen

#### #1 Login screen
- Allow user to enter an email address to login

#### #2 Verification screen
- Allow users to enter OTP code to verify the email

#### #3 Home screen
- Indicate user has logged in successfully

#### Other features
- Write unit test and UI test
- Gitlab-ci